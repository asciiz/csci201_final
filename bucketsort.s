.data 

array: .word 10, 42, 63, 74, 84, 5, 6, 27, 38, -47, 55, 66, 17, 4, 150

.align 8
b1: .space 15

.align 6
b2: .space 15 

.align 6
b3: .space 15 

.align 5
b4: .space 15 

.align 5
b5: .space 15 

.align 7
sorted: .space 15

.align 7
coma: .asciiz "  "
minprompt: .asciiz "minimum value: "
maxprompt: .asciiz "maximum value: "
space: .asciiz " "

.align 4
testprompt: .asciiz "   Test: "


.text

main: 
	li $s0, 15	#size of the array 

	la $t0, array #loading the base address of the aray

	#finding the minimum and maximum value in the list

	lw $t1, 0($t0) #first item of array for compairing minimum 
	lw $t5, 0($t0) #first item of array for compairing the maximum

	li $t4, 0 #for iterating through the array 

	checkminmax: 

		lw $t2, 0($t0)

		slt $t3, $t2, $t1
		beq $t3, 1, min

		slt $t3, $t2, $t5
		beq $t3, 0, max

		j check 
		
		min:
			addi $t1, $t2, 0	#updaing the minimum value in the list
			j check

		max:
			addi $t5, $t2, 0	#updating the maximum value in the list
			j check

		check:
			
			addi $t0, $t0, 4
			addi $t4, $t4, 1

			slt $t3, $t4, $s0
			beq $t3, 1, checkminmax

	li $v0, 4
	la $a0, minprompt
	syscall

	li $v0, 1
	la $a0, ($t1)
	syscall

	li $v0, 4
	la $a0, maxprompt
	syscall

	li $v0, 1
	la $a0, ($t5)
	syscall
	
	#t1 = minimum value	$t5 = max value		$s1 = size of array
	#creating buckets 
	
	sub $t7, $t5, $t1	#t2 = range of numeber avilable int he array 
	
	div $t7, $t7, 5		#to find the range of number that each bucket will hold 
		
	#set the range for each buckets
	addi $s1, $t1, 0 #min value
	add $s2, $s1, $t7
	add $s3, $s2, $t7
	add $s4, $s3, $t7 
	add $s5, $s4, $t7
	move $s6, $t5	#max value
	addi $s6, $s6, 1 #for the edge case af the last value
		
	#adding in buckets
	li $t5, 0 #for bucket1 
	li $t6, 0 #for bucket2
	li $t7, 0 #for bucket3
	li $t8, 0 #for bucket4
	li $t9, 0 #for bucket5
	
	la $t0, array
	li $t4, 0
	
	addtobucket:
	
		lw $t1, 0($t0)
					
		slt $t2, $t1, $s2
		beq $t2, 1, bucket1
		
		slt $t2, $t1, $s3
		beq $t2, 1, bucket2
		
		slt $t2, $t1, $s4
		beq $t2, 1, bucket3
		
		slt $t2, $t1, $s5
		beq $t2, 1, bucket4
		 
		slt $t2, $t1, $s6
		beq $t2, 1, bucket5
	
	bucket1: 
		sw $t1, b1($t5)
		addi $t5, $t5, 4		
		j checkforbucket
		
	bucket2:	
		sw $t1, b2($t6)
		addi $t6, $t6, 4
		j checkforbucket

	bucket3:
		sw $t1, b3($t7)
		addi $t7, $t7, 4
		j checkforbucket
	
	bucket4:
		sw $t1, b4($t8)
		addi $t8, $t8, 4
		j checkforbucket

	bucket5:	
		sw $t1, b5($t9)
		addi $t9, $t9, 4 
		j checkforbucket
	
	checkforbucket:
	
		addi $t0, $t0, 4
		addi $t4, $t4, 1

		slt $t3, $t4, $s0
		beq $t3, 1, addtobucket
	
	
	div $t5, $t5, 4 #number of item in bucket1 
	div $t6, $t6, 4 #for bucket2
	div $t7, $t7, 4 #for bucket3 
	div $t8, $t8, 4 #for bucket4 
	div $t9, $t9, 4 #for bucket5 

	li $v0, 4
	la $a0, testprompt
	syscall
	
	li $v0, 1
	la $a0, ($t5)
	syscall
	li $v0, 4
	la $a0, coma
	syscall
	li $v0, 1
	la $a0, ($t6)
	syscall
	li $v0, 4
	la $a0, coma
	syscall
	li $v0, 1
	la $a0, ($t7)
	syscall
	li $v0, 4
	la $a0, coma
	syscall
	li $v0, 1
	la $a0, ($t8)
	syscall
	li $v0, 4
	la $a0, coma
	syscall
	li $v0, 1
	la $a0, ($t9)
	syscall		
	li $v0, 4
	la $a0, coma
	syscall	
###############################
	################need to sort			
	la $s1, b1
	
	la $a0, b1
	addi $a1, $t5, 0

	jal bubblesort



	la $s2, b2
	
	la $a0, b2
	addi $a1, $t6, 0

	jal bubblesort
	
	la $s3, b3

	la $a0, b3
	addi $a1, $t7, 0

	jal bubblesort
	
	la $s4, b4

	la $a0, b4
	addi $a1, $t8, 0

	jal bubblesort
	
	la $s5, b5

	la $a0, b5
	addi $a1, $t9, 0

	jal bubblesort
	
	#merging the buckets together
	la $a3, sorted
	li $t0, 0 #counter
	addbucket1:
		lw $t1, 0($s1) 
		sw $t1, 0($a3)
		addi $a3, $a3, 4
		addi $s1, $s1, 4
		addi $t0, $t0, 1
		slt $t2, $t0, $t5
		beq $t2, 1, addbucket1

		


	
	li $t0, 0
	addbucket2:
		lw $t1, 0($s2) 
		sw $t1, 0($a3)
		addi $a3, $a3, 4
		addi $s2, $s2, 4
		addi $t0, $t0, 1
		slt $t2, $t0, $t6
		beq $t2, 1, addbucket2
	
	li $t0, 0
	addbucket3:
		lw $t1, 0($s3) 
		sw $t1, 0($a3)
		addi $a3, $a3, 4
		addi $s3, $s3, 4
		addi $t0, $t0, 1
		slt $t2, $t0, $t7
		beq $t2, 1, addbucket3
	
	li $t0, 0
	addbucket4:
		lw $t1, 0($s4) 
		sw $t1, 0($a3)
		addi $a3, $a3, 4
		addi $s4, $s4, 4
		addi $t0, $t0, 1
		slt $t2, $t0, $t8
		beq $t2, 1, addbucket4	
		
	li $t0, 0
	addbucket5:
		lw $t1, 0($s5) 
		sw $t1, 0($a3)
		addi $a3, $a3, 4
		addi $s5, $s5, 4
		addi $t0, $t0, 1
		slt $t2, $t0, $t9
		beq $t2, 1, addbucket5


	
	#final printing 
	la $a1, sorted
	li $t0, 0 #count for printing final list

	li $s0, 15

	li $v0, 4
	la $a0, testprompt
	syscall	
	
	printloop: 
		lw $t1, 0($a1) 
		addi $a1, $a1, 4
		addi $t0, $t0, 1
		
		li $v0, 1
		la $a0, ($t1)
		syscall 
		
		li $v0, 4
		la $a0, coma
		syscall
		
		
		slt $t2, $t0, $s0
		beq $t2, 1, printloop
		
				

li $v0, 10 
syscall

bubblesort:
	
	move $t0, $a0
	move $t2, $a1
	
	addi $t2, $t2, -1 				#decrease size by 1
	li $s7, 0 				    	#set $t6 to 0
	move $k0, $t2
	sll $k0, $k0, 2
	addi $s7, $k0, 0

LOOP1:
	beqz $t2, DONE1
	move $s0, $t2
	
	 

	LOOP2:

		beqz $s0, DONE2
		
		lw $t3, 0($t0)             #loading element of first and next index
		lw $t4, 4($t0)

		slt $t1, $t3, $t4
		bne $t1, $zero, cont

		sw $t3, 4($t0)
		sw $t4, 0($t0)

		cont:

		

  		addi $t0, $t0, 4         #shifting array
  		
  		addi $s0, $s0, -1

  		j LOOP2
	DONE2:
	addi $t2, $t2, -1
	
	sub $t0, $t0, $s7         ########need to reset array
	addi $s7, $s7, -4
	j LOOP1
DONE1:

li $v0, 4
la $a0, space 
syscall
li $v0, 4
la $a0, space 
syscall


move $t2, $a1
la $s6, array

print:                      #storing in array
beqz $t2, finish 
lw $t3, 0($t0)
sw $t3, 0($s6)


# li $v0, 1 				   #printing
# la $a0, ($t3) 
# syscall

# li $v0, 4
# la $a0, space 
# syscall

addi $t0, $t0, 4
addi $s6, $s6, 4
addi $t2, $t2, -1
j print
finish:
la $v0, array
jr $ra
# print1:
# lw $t2, 0($t1)
# la $t5, array
#                    #printi
# beqz $t2, finish 
# lw $t3, 0($t0)


# li $v0, 1 				   #printing
# la $a0, ($t3) 
# syscall

# li $v0, 4
# la $a0, space 
# syscall

# addi $t0, $t0, 4
# addi $t5, $t5, 4
# addi $t2, $t2, -1
# j print1